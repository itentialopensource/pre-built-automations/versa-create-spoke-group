## _Deprecation Notice_
This Pre-Built has been deprecated as of 09-01-2024 and will be end of life on 09-01-2025. The capabilities of this Pre-Built have been replaced by the [Versa - REST](https://gitlab.com/itentialopensource/pre-built-automations/versa-rest)

<!-- This is a comment in md (Markdown) format, it will not be visible to the end user -->

<!-- Update the below line with your Pre-Built name -->
# Versa Create Spoke Group

<!-- Leave TOC intact unless you've added or removed headers -->
## Table of Contents

* [Overview](#overview)
* [Requirements](#requirements)
* [Features](#features)
* [How to Install](#how-to-install)
* [How to Run](#how-to-run)
* [Additional Information](#additional-information)

## Overview

This pre-built is used to create a new spoke group to Versa Director which can then be used during spoke template creation.
<!-- Write a few sentences about the Pre-Built and explain the use case(s) -->
<!-- Avoid using the word Artifact. Please use Pre-Built, Pre-Built Transformation or Pre-Built Automation -->
<!-- Ex.: The Migration Wizard enables IAP users to conveniently move their automation use cases between different IAP environments -->
<!-- (e.g. from Dev to Pre-Production or from Lab to Production). -->

<!-- Workflow(s) Image Placeholder - TO BE ADDED DIRECTLY TO GitLab -->
<!-- REPLACE COMMENT BELOW WITH IMAGE OF YOUR MAIN WORKFLOW -->
<!--

-->
<!-- ADD ESTIMATED RUN TIME HERE -->
<!-- e.g. Estimated Run Time: 34 min. -->
_Estimated Run Time_: 1 minute

## Requirements

This Pre-Built requires the following:

<!-- Unordered list highlighting the requirements of the Pre-Built -->
<!-- EXAMPLE -->
<!-- * cisco ios device -->
<!-- * Ansible or NSO (with F5 NED) * -->
* Itential Automation Platform
  * `^2023.1.x`
* A running instance of the Itential OpenSource Versa Director adapter, which can be installed from [here](https://gitlab.com/itentialopensource/adapters/controller-orchestrator/adapter-versa_director).

## Features

The main benefits and features of the Pre-Built are outlined below.

Every Itential Pre-built is designed to optimize network performance and configuration by focusing on reliability, flexibility and coordination of network changes with applications and IT processes. As the network evolves, Pre-builts allow customers to focus on effective management and compliance to minimize risks of disruption and outage that can negatively impact quality of service.

<!-- Unordered list highlighting the most exciting features of the Pre-Built -->
<!-- EXAMPLE -->
<!-- * Automatically checks for device type -->
<!-- * Displays dry-run to user (asking for confirmation) prior to pushing config to the device -->
<!-- * Verifies downloaded file integrity (using md5), will try to download again if failed -->


## How to Install

* Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Requirements](#requirements) section in order to install the Pre-Built. 
* The Pre-Built can be installed from within App-Admin_Essential. Simply search for the name of your desired Pre-Built and click the install button (as shown below).

<!-- OPTIONAL - Explain if external components are required outside of IAP -->
<!-- Ex.: The Ansible roles required for this Pre-Built can be found in the repository located at https://gitlab.com/itentialopensource/pre-built-automations/hello-world -->

## How to Run

Use the following to run the Pre-Built:


The pre-built can be run as a standalone or as part of a parent job. 
- To run it as a standalone job, run the respective job from Operations Manager with the necessary json form. 
- The workflow or the job can also be used as part of another parent job.

The description of the formdata needed by the prebuilt is given below:
The required values to be passed on are:

**name:** Name of the spoke group to be created.\
**org:** Name of the parent organization for which the spoke group is to be created.\
**hubType:** Choose between Hub / Hub Controller.\
**region:** The region in which the Spokes are placed.

**name:** The specific routing instance on the devices in the Spoke Group for which the Spoke Group Type needs to be mapped.\
**groupType:** The type of Spoke group type that the spoke group routing instance will fall within. Currently, only 3 choices are available to select from.\
**communityRef:** Applicable to Spoke to Spoke Direct communication, this represents the BGP community value that the user needs to be use for Spoke to Spoke Direct communication for proper BGP route import/export policies.\
**priority of hubs:** Choice between 1 through 8, used to configure in which order will the Hubs be chosen by the Spoke when it contacts another Spoke in a Spoke o Spoke Direct model.
<!-- Explain the main entrypoint(s) for this Pre-Built: Automation Catalog item, Workflow, Postman, etc. -->

## Versa instance

The above created service template can be verified on the versa instance by logging in using the credentials provided.

After logging in, select the workflows tab, choose the 'spoke groups' from the left pane to view all the spoke groups created.

## Additional Information

Please use your Itential Customer Success account if you need support when using this Pre-Built.
